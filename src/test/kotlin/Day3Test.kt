import Day3.getColumnIndex
import org.amshove.kluent.`should be equal to`
import org.jetbrains.kotlinx.multik.api.mk
import org.jetbrains.kotlinx.multik.api.zeros
import org.junit.jupiter.api.Test

internal class Day3Test {

    @Test
    fun `test solve first star example issue`() {
        val result = Day3.solveFirstStar(inputPath = "Day3.test.in")
        result `should be equal to` 7
    }

    @Test
    fun `test solve first star issue`() {
        val result = Day3.solveFirstStar(inputPath = "Day3.in")
        result `should be equal to` 254
    }

    @Test
    fun `test solve second star example issue`() {
        val result = Day3.solveSecondStar(inputPath = "Day3.test.in")
        result `should be equal to` 336
    }

    @Test
    fun `test solve second star issue`() {
        val result = Day3.solveSecondStar(inputPath = "Day3.in")
        result `should be equal to` 1666768320
    }

    @Test
    fun `test overflowing column index calculation`() {
        val matrix = mk.zeros<Double>(4, 3)

        matrix.getColumnIndex(0) `should be equal to` 0
        matrix.getColumnIndex(1) `should be equal to` 1
        matrix.getColumnIndex(2) `should be equal to` 2

        matrix.getColumnIndex(3) `should be equal to` 0
        matrix.getColumnIndex(4) `should be equal to` 1
        matrix.getColumnIndex(5) `should be equal to` 2

        matrix.getColumnIndex(6) `should be equal to` 0
        matrix.getColumnIndex(7) `should be equal to` 1
        matrix.getColumnIndex(8) `should be equal to` 2

    }
}