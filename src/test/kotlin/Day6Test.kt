import org.amshove.kluent.`should be equal to`
import org.junit.jupiter.api.Test

internal class Day6Test {

    @Test
    fun `test solve first star example issue`() {
        val result = Day6.solveFirstStar(inputPath = "Day6.test.in")
        result `should be equal to` 11
    }

    @Test
    fun `test solve first star issue`() {
        val result = Day6.solveFirstStar(inputPath = "Day6.in")
        result `should be equal to` 6748
    }

    @Test
    fun `test solve second star example issue`() {
        val result = Day6.solveSecondStar(inputPath = "Day6.test.in")
        result `should be equal to` 6
    }

    @Test
    fun `test solve second star issue`() {
        val result = Day6.solveSecondStar(inputPath = "Day6.in")
        result `should be equal to` 3445
    }
}