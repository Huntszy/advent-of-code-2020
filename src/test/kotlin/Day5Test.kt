import org.amshove.kluent.`should be equal to`
import org.junit.jupiter.api.Test

internal class Day5Test {

    @Test
    fun `test solve first star example issue`() {
        val result = Day5.solveFirstStar(inputPath = "Day5.test.in")
        result `should be equal to` 820
    }

    @Test
    fun `test solve first star issue`() {
        val result = Day5.solveFirstStar(inputPath = "Day5.in")
        result `should be equal to` 888
    }

    @Test
    fun `test solve second star issue`() {
        val result = Day5.solveSecondStar(inputPath = "Day5.in")
        result `should be equal to` 522
    }
}