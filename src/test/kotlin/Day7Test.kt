import org.amshove.kluent.`should be equal to`
import org.amshove.kluent.`should match at least one of`
import org.junit.jupiter.api.Test

internal class Day7Test {

    @Test
    fun `test solve first star example issue`() {
        val result = Day7.solveFirstStar(inputPath = "Day7.test.in")
        result `should be equal to` 4
    }

    @Test
    fun `test solve first star issue`() {
        val result = Day7.solveFirstStar(inputPath = "Day7.in")
        result `should be equal to` 335
    }

    @Test
    fun `test solve second star example issue`() {
        val result = Day7.solveSecondStar(inputPath = "Day7.test.in")
        result `should be equal to` 32
    }

    @Test
    fun `test solve second star issue`() {
        val result = Day7.solveSecondStar(inputPath = "Day7.in")
        result `should be equal to` 2431
    }

    @Test
    fun `read input test`() {
        val rules = Day7.readInput("Day7.read_input.test.in")

        rules.size `should be equal to` 1

        val rule = rules[0]
        rule.outerColor `should be equal to` "shiny tan"
        rule.content.size `should be equal to` 3
        rule.content `should match at least one of` { it.first == 5 && it.second == "plaid silver" }
        rule.content `should match at least one of` { it.first == 13 && it.second == "light lavender" }
        rule.content `should match at least one of` { it.first == 4 && it.second == "wavy purple" }
    }
}