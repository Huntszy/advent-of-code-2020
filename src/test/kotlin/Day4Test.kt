import org.amshove.kluent.`should be equal to`
import org.junit.jupiter.api.Test

internal class Day4Test {

    @Test
    fun `test solve first star example issue`() {
        val result = Day4.solveFirstStar(inputPath = "Day4.test.in")
        result `should be equal to` 2
    }

    @Test
    fun `test solve first star issue`() {
        val result = Day4.solveFirstStar(inputPath = "Day4.in")
        result `should be equal to` 239
    }

    @Test
    fun `test solve second star example issues`() {
        val invalids = Day4.solveSecondStar(inputPath = "Day4_Part2_all_invalid.test.in")
        invalids `should be equal to` 0

        val valids = Day4.solveSecondStar(inputPath = "Day4_Part2_all_valid.test.in")
        valids `should be equal to` 4
    }

    @Test
    fun `test solve second star issue`() {
        val invalids = Day4.solveSecondStar(inputPath = "Day4.in")
        invalids `should be equal to` 188
    }
}