import org.amshove.kluent.`should be equal to`
import org.junit.jupiter.api.Test

internal class Day1Test {

    @Test
    fun `test solve first star issue`() {
        val result = Day1.solveFirstStar(inputPath = "Day1.in", targetSum = 2020)
        result `should be equal to` 365619
    }

    @Test
    fun `test solve second star issue`() {
        val result = Day1.solveSecondStar(inputPath = "Day1.in", targetSum = 2020)
        result `should be equal to` 236873508
    }
}