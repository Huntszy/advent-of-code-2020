import org.amshove.kluent.`should be equal to`
import org.junit.jupiter.api.Test

internal class Day2Test {
    @Test
    fun `test solve first star issue`() {
        val result = Day2.solveFirstStar(inputPath = "Day2.in")
        result `should be equal to` 548
    }

    @Test
    fun `test solve second star issue`() {
        val result = Day2.solveSecondStar(inputPath = "Day2.in")
        result `should be equal to` 502
    }
}