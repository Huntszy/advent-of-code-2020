object Day1 {
    fun solveFirstStar(inputPath: String, targetSum: Int): Int? {
        val input = readInput(inputPath)
        val lookUp = input.toSet()

        return input
            .firstOrNull { lookUp.contains(targetSum - it) }
            ?.let { it * (targetSum - it) }
    }

    fun solveSecondStar(inputPath: String, targetSum: Int): Int? {
        val input = readInput(inputPath)

        val resultTerms = input.mapIndexed { term1Index, term1 ->
            input.drop(term1Index + 1).mapIndexed { term2Index, term2 ->
                input.drop(term1Index + term2Index + 2)
                    .firstOrNull { term3 -> term1 + term2 + term3 == targetSum }
                    ?.let { term3 -> listOf(term1, term2, term3) }
            }
                .firstOrNull { it != null }
        }.firstOrNull { it != null }

        return resultTerms?.reduceOrNull { acc, i -> acc * i }
    }

    private fun readInput(inputPath: String): List<Int> =
        this.javaClass.classLoader.getResourceAsStream(inputPath)
            ?.use { inputStream ->
                inputStream.reader()
                    .readLines()
                    .mapNotNull { line ->
                        line.toIntOrNull()
                    }
            }
            ?: emptyList()
}