import org.jetbrains.kotlinx.multik.api.mk
import org.jetbrains.kotlinx.multik.api.ndarray
import org.jetbrains.kotlinx.multik.ndarray.data.D2
import org.jetbrains.kotlinx.multik.ndarray.data.D2Array
import org.jetbrains.kotlinx.multik.ndarray.data.NDArray
import org.jetbrains.kotlinx.multik.ndarray.data.get

object Day3 {

    private const val emptySpot = 0
    private const val emptySpotChar = '.'
    private const val tree = 1
    private const val treeChar = '#'

    fun solveFirstStar(inputPath: String): Int {
        val map = readInput(inputPath)
        val slope = Slope(3, 1)
        return sumEncounteredTrees(map, slope, 0, 0)
    }

    fun solveSecondStar(inputPath: String): Int {
        val map = readInput(inputPath)
        val slopes = listOf(
            Slope(1, 1),
            Slope(3, 1),
            Slope(5, 1),
            Slope(7, 1),
            Slope(1, 2)
        )
        return slopes.map { sumEncounteredTrees(map, it, 0, 0) }
            .reduce { acc, element -> acc * element }
    }

    private fun sumEncounteredTrees(map: NDArray<Int, D2>, slope: Slope, row: Int, column: Int): Int =
        if (row >= map.shape[0]) {
            0
        } else {
            val currentSpot = if (map[row, map.getColumnIndex(column)] == tree) 1 else 0

            currentSpot + sumEncounteredTrees(
                map = map,
                slope,
                row = row + slope.vertivalDisplacement,
                column = column + slope.horizontalDisplacement
            )
        }


    private fun readInput(inputPath: String): D2Array<Int> {
        val charArraysOfArray = this.javaClass.classLoader.getResourceAsStream(inputPath)
            ?.use { it ->
                it.reader()
                    .readLines()
                    .map { line -> line.replace(emptySpotChar, emptySpot.toChar()) }
                    .map { line -> line.replace(treeChar, tree.toChar()) }
                    .map { line -> line.toList() }
                    .map { line -> line.map { spot -> spot.code } }
            }
            ?: emptyList()

        return mk.ndarray(charArraysOfArray)
    }

    private data class Slope(val horizontalDisplacement: Int, val vertivalDisplacement: Int)

    fun D2Array<*>.getColumnIndex(position: Int) = position % this.shape[1]

}
