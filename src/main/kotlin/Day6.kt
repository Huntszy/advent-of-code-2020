object Day6 {

    fun solveFirstStar(inputPath: String): Int = readInput(inputPath)
        .map { it.replace("""\s""".toRegex(), "") }
        .sumOf { it.toSet().size }

    fun solveSecondStar(inputPath: String): Int = readInput(inputPath)
        .sumOf { countAllYesInGroup(it) }

    private fun countAllYesInGroup(groupAnswer: String): Int = groupAnswer
        .lines()
        .map { it.toSet() }
        .reduce { acc, answers -> acc.intersect(answers) }
        .size


    private fun readInput(inputPath: String) =
        this.javaClass.classLoader.getResourceAsStream(inputPath)
            ?.use { it ->
                it.reader()
                    .readText()
                    .split(Regex(EMPTY_LINES_REGEX, RegexOption.MULTILINE))
                    .filter { it.isNotBlank() }
                    .map { it.trim() }
            }
            ?: emptyList()
}