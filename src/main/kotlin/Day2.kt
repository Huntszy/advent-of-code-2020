object Day2 {
    private val regex = """(\d+)-(\d+) (\w): (\w+)""".toRegex()

    fun solveFirstStar(inputPath: String) = readInput(inputPath).count { it.isValidByOldPolicy }

    fun solveSecondStar(inputPath: String) = readInput(inputPath).count { it.isValidByNewPolicy }

    private fun readInput(inputPath: String): List<PasswordEntry> =
        this.javaClass.classLoader.getResourceAsStream(inputPath)?.use { it ->
            it.reader()
                .readLines()
                .mapNotNull { line -> regex.find(line)?.destructured }
                .map { (min, max, char, password) -> PasswordEntry(min.toInt()..max.toInt(), char.first(), password) }
        }
            ?: emptyList()

    data class PasswordEntry(
        val letterRange: IntRange,
        val letter: Char,
        val password: String
    )

    private val PasswordEntry.isValidByOldPolicy get() = password.count { it == letter } in letterRange

    private val PasswordEntry.isValidByNewPolicy
        get() = (password[letterRange.first - 1] == letter) xor (password[letterRange.last - 1] == letter)

}