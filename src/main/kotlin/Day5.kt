import kotlin.math.ceil
import kotlin.math.floor

private const val FRONT = 'F'
private const val BACK = 'B'
private const val LEFT = 'L'
private const val RIGHT = 'R'

object Day5 {

    fun solveFirstStar(inputPath: String): Int = readInput(inputPath)
        .map { decodeSeatPosition(it) }
        .maxOfOrNull { getSeatId(it) } ?: throw Exception("No seat ids have been decoded")

    fun solveSecondStar(inputPath: String): Int {
        val reservedSeatIds = readInput(inputPath).map { decodeSeatPosition(it) }.map { getSeatId(it) }
        return (0..128 * 8)
            .filterNot { reservedSeatIds.contains(it) }
            .singleOrNull { reservedSeatIds.contains(it - 1) && reservedSeatIds.contains(it + 1) }
            ?: throw Exception("No or too much empty seat found")
    }

    private fun decodeSeatPosition(binaryPartitioned: String): Pair<Int, Int> {
        val row = decodeDimension(binaryPartitioned.substring(0..6), 0..127, BACK, FRONT)
        val column = decodeDimension(binaryPartitioned.substring(7..9), 0..7, RIGHT, LEFT)

        return row to column
    }

    private fun decodeDimension(binaryPartitionedRow: String, partition: IntRange, upper: Char, lower: Char): Int =
        if (binaryPartitionedRow.length > 1) {
            when (binaryPartitionedRow.first()) {
                upper -> decodeDimension(
                    binaryPartitionedRow.drop(1),
                    partition.upperHalf,
                    upper,
                    lower
                )
                lower -> decodeDimension(
                    binaryPartitionedRow.drop(1),
                    partition.lowerHalf,
                    upper,
                    lower
                )
                else -> throw Exception("Unspecified dimension character found")
            }
        } else {
            when (binaryPartitionedRow.first()) {
                upper -> partition.last
                lower -> partition.first
                else -> throw Exception("Unspecified dimension character found")
            }
        }

    private fun readInput(inputPath: String) =
        this.javaClass.classLoader.getResourceAsStream(inputPath)
            ?.use {
                it.reader()
                    .readLines()
            }
            ?: emptyList()

    private fun getSeatId(seatPosition: Pair<Int, Int>) = seatPosition.first * 8 + seatPosition.second
}

private val IntRange.distance get() = this.last - this.first

private val IntRange.upperHalf get() = ceil(this.first + this.distance / 2.0).toInt()..this.last

private val IntRange.lowerHalf get() = this.first..floor(this.last - this.distance / 2.0).toInt()