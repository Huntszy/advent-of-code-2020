import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

private val VALID_EYE_COLORS = listOf("amb", "blu", "brn", "gry", "grn", "hzl", "oth")

object Day4 {

    fun solveFirstStar(inputPath: String) = readInput(inputPath).count { it.isValidNorthPoleCredential }

    fun solveSecondStar(inputPath: String) = readInput(inputPath).count { it.isValidPassport }

    private fun readInput(inputPath: String) =
        this.javaClass.classLoader.getResourceAsStream(inputPath)
            ?.use { it ->
                it.reader()
                    .readText()
                    .split(Regex(EMPTY_LINES_REGEX, RegexOption.MULTILINE))
                    .filter { it.isNotBlank() }
                    .map { rawPassport ->
                        rawPassport.split("(\\s)".toRegex()).filter { it.isNotBlank() }.associate { rawField ->
                            val (key, value) = rawField.split(':')
                            key to value
                        }
                    }
                    .map { passportMap -> Passport(passportMap) }
            }
            ?: emptyList()

}

class Passport(map: Map<String, String>) {
    private val defaultMap = map.withDefault { null }

    val byr: SelfCheckingField<String?> by SelfCheckingDelegate(map = defaultMap) { value ->
        (1920..2002).contains(value?.toInt())
    }
    val iyr: SelfCheckingField<String?> by SelfCheckingDelegate(map = defaultMap) { value ->
        (2010..2020).contains(value?.toInt())
    }
    val eyr: SelfCheckingField<String?> by SelfCheckingDelegate(map = defaultMap) { value ->
        (2020..2030).contains(value?.toInt())
    }
    val hgt: SelfCheckingField<String?> by SelfCheckingDelegate(map = defaultMap) { value -> validateHeight(value) }
    val hcl: SelfCheckingField<String?> by SelfCheckingDelegate(map = defaultMap) { value -> validateHairColor(value) }
    val ecl: SelfCheckingField<String?> by SelfCheckingDelegate(map = defaultMap) { value ->
        VALID_EYE_COLORS.contains(value)
    }
    val pid: SelfCheckingField<String?> by SelfCheckingDelegate(map = defaultMap) { value ->
        value?.let {
            value.length == 9 && value.all { it in ('0'..'9') }
        } ?: false
    }
    val cid: SelfCheckingField<String?> by SelfCheckingDelegate(map = defaultMap) { value -> true }

    override fun toString(): String {
        return """
                        byr: $byr 
                        iyr: $iyr
                        eyr: $eyr
                        hgt: $hgt
                        hcl: $hcl
                        ecl: $ecl
                        pid: $pid
                        cid: $cid
                     """.trimIndent()

    }
}

class SelfCheckingField<T>(val value: T, val validityCondition: (T) -> Boolean) {
    val isValid: Boolean get() = this.validityCondition(value)
}

class SelfCheckingDelegate<T>(val map: Map<String, T>, private val validityCondition: (T?) -> Boolean) :
    ReadOnlyProperty<Passport, SelfCheckingField<T?>> {

    override fun getValue(thisRef: Passport, property: KProperty<*>): SelfCheckingField<T?> {
        return SelfCheckingField(map[property.name], validityCondition)
    }

}

private fun validateHeight(height: String?): Boolean =
    height
        ?.let {
            """(\d+)(cm|in)""".toRegex().find(height)?.destructured
        }
        ?.let { (value, unit) ->
            when (unit) {
                "cm" -> (150..193).contains(value.toInt())
                "in" -> (59..76).contains(value.toInt())
                else -> false
            }
        }
        ?: false

private fun validateHairColor(hairColor: String?): Boolean =
    hairColor?.matches("""#([0-9a-f]{6})""".toRegex())
        ?: false

private val Passport.isValidNorthPoleCredential
    get() = listOf(
        byr,
        iyr,
        eyr,
        hgt,
        hcl,
        ecl,
        pid
    ).none { it.value == null }

private val Passport.isValidPassport
    get() = listOf(
        byr,
        iyr,
        eyr,
        hgt,
        hcl,
        ecl,
        pid,
        cid
    ).all { it.isValid }
