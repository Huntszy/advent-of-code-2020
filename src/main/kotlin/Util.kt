const val EMPTY_LINES_REGEX = """((\r\n|\n|\r)${'$'})|(^(\r\n|\n|\r))|^\s*${'$'}"""


inline fun <T> timed(functionName: String = "Anonymous", f: () -> T): T {
    val startTime = System.currentTimeMillis()
    val result = f()
    println("It's time! $functionName has ran for ${System.currentTimeMillis() - startTime} millis")
    return result
}