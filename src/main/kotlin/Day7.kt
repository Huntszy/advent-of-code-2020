private val OUTER_BAG_REGEX = """([\w\s]+) bags contain""".toRegex()
private val CONTENT_REGEX = """(\d+) ([\w\s]+)""".toRegex()
private const val COLOR_TO_CHECK = "shiny gold"

object Day7 {

    fun solveFirstStar(inputPath: String) =
        readInput(inputPath)
            .run { getWrapperColors(this, COLOR_TO_CHECK) }
            .run { this.size }

    fun solveSecondStar(inputPath: String) =
        readInput(inputPath)
            .run { getInnerBagsCount(this, COLOR_TO_CHECK) }

    private fun getInnerBagsCount(rules: List<Rule>, colorToCheck: String): Int {
        val directContent = getDirectContent(rules, colorToCheck)
        val directContentBagCount = directContent.sumOf { it.first }

        return if (directContent.isEmpty()) {
            0
        } else {
            directContent.map {
                it.first * getInnerBagsCount(rules, it.second)
            }.fold(directContentBagCount) { sum, element ->
                sum + element
            }
        }
    }

    private fun getDirectContent(rules: List<Rule>, colorToCheck: String) =
        rules.first { rule -> rule.outerColor == colorToCheck }.content


    private fun getWrapperColors(rules: List<Rule>, colorToCheck: String): Set<String> {
        val directWrappers = getDirectWrappers(rules, colorToCheck)

        return if (directWrappers.isEmpty()) {
            emptySet()
        } else {
            directWrappers.union(
                directWrappers.map {
                    getWrapperColors(rules, it)
                }.reduce { acc, wrapperColors -> acc.union(wrapperColors) }
            )
        }
    }

    private fun getDirectWrappers(rules: List<Rule>, colorToCheck: String): Set<String> =
        rules
            .filter { rule ->
                rule.content
                    .any { content -> content.second == colorToCheck }
            }.map { it.outerColor }
            .toSet()


    fun readInput(inputPath: String) =
        this.javaClass.classLoader.getResourceAsStream(inputPath)
            ?.use { it ->
                it.reader()
                    .readLines()
                    .map { parseRule(it) }
            }
            ?: emptyList()

    private fun parseRule(rawRule: String): Rule {
        val outerColor = OUTER_BAG_REGEX.find(rawRule)
            ?.value
            ?.dropLast(" bags contain".length)
            ?: throw Exception("Outer color extraction failed")

        val content = CONTENT_REGEX.findAll(rawRule)
            .map { it.destructured }
            .map { (occurrences, bagColor) ->
                occurrences.toInt() to bagColor.removeSuffix(" bag").removeSuffix(" bags")
            }
            .toList()

        return Rule(outerColor, content)
    }
}

data class Rule(
    val outerColor: String,
    val content: List<Pair<Int, String>>
)